let _startNode;

function selectFirstChild() {
  let elem;
  if (_startNode) {
    elem = _startNode;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem = document.querySelector('#list').firstElementChild;
    elem.style.color = 'beige';
    elem.style.backgroundColor = 'green';
    _startNode = elem;
  } else {
    elem = document.querySelector('#list').firstElementChild;
    elem.style.color = 'beige';
    elem.style.backgroundColor = 'green';
    _startNode = elem;
  }
}



function selectLastChild() {
  let elem;
  if (_startNode) {
    elem = _startNode;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem = document.querySelector('#list').lastElementChild;
    elem.style.color = 'beige';
    elem.style.backgroundColor = 'green';
    _startNode = elem;
  } else {
    elem = document.querySelector('#list').lastElementChild;
    elem.style.color = 'beige';
    elem.style.backgroundColor = 'green';
    _startNode = elem;
  }
}


function selectNextNode() {
  let elem;
  if (!_startNode) {
    elem = document.querySelector('#list').firstElementChild;
    elem.style.color = 'beige';
    elem.style.backgroundColor = 'green';
    _startNode = elem;
  } else if (_startNode == document.querySelector('#list').lastElementChild) {
    elem = _startNode;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem = document.querySelector('#list').firstElementChild;
    elem.style.color = 'beige';
    elem.style.backgroundColor = 'green';
    _startNode = elem;
  } else {
    elem = _startNode;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem = elem.nextElementSibling;
    elem.style.color = 'beige';
    elem.style.backgroundColor = 'green';
    _startNode = elem;
  }
}

function selectPrevNode() {
  let elem;
  if (!_startNode) {
    elem = document.querySelector('#list').lastElementChild;
    elem.style.color = 'beige';
    elem.style.backgroundColor = 'green';
    _startNode = elem;
  } else if (_startNode == document.querySelector('#list').firstElementChild) {
    elem = _startNode;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem = document.querySelector('#list').lastElementChild;
    elem.style.color = 'beige';
    elem.style.backgroundColor = 'green';
    _startNode = elem;
  } else {
    elem = _startNode;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem = elem.previousElementSibling;
    elem.style.color = 'beige';
    elem.style.backgroundColor = 'green';
    _startNode = elem;
  }
}

function createNewChild() {
  const elem = document.querySelector('#list');
  let liText = elem.lastElementChild.innerHTML;
  liText = liText.split(' ').pop();
  const newLi = document.createElement("li");
  newLi.innerText = `List Item ${++liText}`;
  elem.append(newLi);
}

function createNewChildAtStart() {
  const elem = document.querySelector('#list');
  let liText = elem.firstElementChild.innerHTML;
  liText = liText.split(' ').pop();
  const newLi = document.createElement("li");
  newLi.innerText = `List Item ${--liText}`;
  elem.prepend(newLi);
}


function removeLastChild() {
  document.querySelector('#list').lastElementChild.remove();
}