// Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.
// Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір тла.
// Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна згенерувати тег div з текстом, який був у багаторядковому полі. багаторядкове поле слід очистити після переміщення інформації
// Створіть картинку та кнопку з назвою "Змінити картинку" зробіть так щоб при завантаженні сторінки була картинка https://itproger.com/img/courses/1476977240.jpg При натисканні на кнопку вперше картинка замінилася на https://itproger.com/img/courses/1476977488.jpg при другому натисканні щоб картинка замінилася на https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png
// Створює на сторінці 10 парахрафів і зробіть так, щоб при натисканні на параграф він зникав
// Попрацювати з файлом 08



// Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.

function appendBody(tag) {
  document.body.append(tag);
}

function inputAdd(amount) {
  for (let i = 0; i < amount; i++) {
    const input = document.createElement('input');
    input.classList.add("input");
    appendBody(input);
  }
}
inputAdd(2);

const changeBtn = document.createElement('button');
changeBtn.innerText = "flip-flop";
changeBtn.onclick = function () { flipFlop(); };
appendBody(changeBtn);

function flipFlop() {
  const [...arr] = document.getElementsByClassName('input');
  if (!arr[0].value || !arr[1].value) { return alert('заполните оба поля'); }
  arr[2] = arr[0].value;
  arr[0].value = arr[1].value;
  arr[1].value = arr[2];
}


// Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір тла.


function divAdd(amount) {
  for (let i = 0; i < amount; i++) {
    const div = document.createElement('div');
    appendBody(div);
  }
}

function divChangeBG() {
  const [...divs] = document.getElementsByTagName('div');
  divs.forEach(elem => elem.style.backgroundColor = 'cyan');
}
divAdd(5);
divChangeBG();


// Створіть багаторядкове поле для введення тексту та кнопки.
// Після натискання кнопки користувачем програма повинна згенерувати тег div з текстом, який був у багаторядковому полі.
// багаторядкове поле слід очистити після переміщення інформації


const textArea = document.createElement('textarea');
const areaBtn = document.createElement('button');
areaBtn.innerText = 'some text';
areaBtn.onclick = function () { generateDiv(); };
appendBody(textArea);
appendBody(areaBtn);


function generateDiv() {
  if (!textArea.value) {
    alert("Заполните текстовое поле");
    return;
  }
  const newDiv = document.createElement('div');
  const elem = areaBtn.nextElementSibling;
  const parentNode = areaBtn.parentNode;
  parentNode.insertBefore(newDiv, elem);
  newDiv.innerText = textArea.value;
  textArea.value = '';
}



// Створіть картинку та кнопку з назвою "Змінити картинку" зробіть так щоб при завантаженні
// сторінки була картинка https://itproger.com/img/courses/1476977240.jpg
// При натисканні на кнопку вперше картинка замінилася на https://itproger.com/img/courses/1476977488.jpg
// при другому натисканні щоб картинка замінилася на https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png



const myPic = document.createElement('img');
myPic.setAttribute('src', 'https://itproger.com/img/courses/1476977240.jpg');
myPic.setAttribute('alt', 'myPic');
const picBtn = document.createElement('button');
picBtn.innerText = 'Заменить картинку';
picBtn.onclick = function () { picChange(); };
appendBody(myPic);
appendBody(picBtn);

function picChange() {
  if ((myPic.getAttribute('src').toString()) == "https://itproger.com/img/courses/1476977240.jpg") {
    myPic.setAttribute('src', 'https://itproger.com/img/courses/1476977488.jpg');
  } else {
    myPic.setAttribute('src', 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png');
  }
}


// Створює на сторінці 10 парахрафів і зробіть так, щоб при натисканні на параграф він зникав


function createPara(amount) {
  for (let i = 0; i < amount; i++) {
    const para = document.createElement('p');
    para.style.backgroundColor = 'lightgray';
    para.innerText = `этот параграф сгенерерован в цикле и имеет номер ${i}`;
    para.onclick = (() => para.remove());
    appendBody(para);
  }
}
createPara(10);